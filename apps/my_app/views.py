from django.shortcuts import render, HttpResponse ,redirect
from django.contrib import messages
from time import gmtime, strftime
from datetime import datetime


# Create your views here.
def index(request):
    now = datetime.now()
    
    year = now.strftime("%Y")
    print("year:", year)

    month = now.strftime("%m")
    print("month:", month)

    day = now.strftime("%d")
    print("day:", day)

    time = now.strftime("%H:%M:%S")
    print("time:", time)

    date_time = now.strftime("%b.%d.%Y")
    print("date is:",date_time)	

    context ={
        "date":date_time,
    }

    return render(request,'index.html', context)

def podcast(request):
    now = datetime.now()
    
    year = now.strftime("%Y")
    print("year:", year)

    month = now.strftime("%m")
    print("month:", month)

    day = now.strftime("%d")
    print("day:", day)

    time = now.strftime("%H:%M:%S")
    print("time:", time)

    date_time = now.strftime("%m/%d/%Y")
    print("date is:",date_time)	

    context ={
        "date":date_time,
    }

    return render(request, 'podcast.html', context)

def subscribe(request):

    return render(request, 'subscribe.html')

def merch(request):

    return render(request, 'merch.html')

def about(request):

    return render(request, 'about.html')

def founders(request):

    return render(request, 'founders.html')

def staff(request):
    
    return render(request, 'staff.html')

def view(request):
    now = datetime.now()
    
    year = now.strftime("%Y")
    print("year:", year)

    month = now.strftime("%m")
    print("month:", month)

    day = now.strftime("%d")
    print("day:", day)

    time = now.strftime("%H:%M:%S")
    print("time:", time)

    date_time = now.strftime("%m/%d/%Y")
    print("date is:",date_time)	

    context ={
        "date":date_time,
    }
    return render(request, 'view.html', context)