from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('subscribe', views.subscribe),
    path('podcast', views.podcast),
    path('merch', views.merch),
    path('about', views.about),
    path('founders', views.founders),
    path('staff', views.staff),
    path('view',views.view),
]